import React, { Component } from 'react';
import './App.css';
import axios from 'axios'

function ListCitySugession(props) {
  const cityLists = props.cityLists;
  const listItems = cityLists.map((city) =>
    <li key={city.id} onClick={() => props.parentForm.changeCityWeather(city.name)}>{city.name}</li>
  );
  return (
    <div id="suggesstion-box" className = "suggesstion-box"><ul>{listItems}</ul></div>
  );
}

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      cityLists: []
    };
  }
  changeCityWeather(cityName){
    this.setState({
      cityLists: [],
      loading: false
    });
    this.props.parentApp.callApiGetCurrentWeather(cityName);
  }
  checkCityWeather(keyword) {
    let self = this;
    self.setState({
      loading: true,
      cityLists: []
    });

    const res = axios.create({
      timeout: 10000,
      withCredentials: false,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'x-www-form-urlencoded',
      },
      params: {
        keyword: keyword
      }
    });

    res.get('http://localhost/WeatherReact/api/suggest-city.php')
      .then(function (response) {
          console.log(response.data);
          self.setState({
            cityLists: response.data,
            loading: false
          });
      })
      .catch(function (error) {
          console.log(error);
      });
  }

  onKeyup(event) {
    if (this.state.loading) {
      return false;
    }
    let keyword = event.target.value;
    this.checkCityWeather(keyword);
  }

  componentDidMount() {
    
  }

  render() {
    return (
      <form method="get">
        <div className="form-group">
          <input type="text" 
            onKeyUp={this.onKeyup.bind(this)}
            className="form-control"
            id="search-box"
            name="city_name"
            placeholder="Country Name"
            autoComplete="off" />
        </div>
        <ListCitySugession cityLists={this.state.cityLists} parentForm={this}/>
      </form>
    );
  }
}
export default Form;

