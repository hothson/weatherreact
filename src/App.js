import React, { Component } from 'react';
import './App.css';
import Form from './Form';
import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentWeather: {
        location: '...',
        bringWhat: '...',
        howAllDay: '...',
        weatherIcon: '...',
        weatherDescription: '...',
        fDegree: '...',
        cDegree: '...',
        humidity: '...',
        windDirection: '...'
      }
    };
  }

  callApiGetCurrentWeather(keyword) {
    console.log('keyword', keyword);
    let self = this;
    const res = axios.create({
      timeout: 10000,
      withCredentials: false,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'x-www-form-urlencoded',
      },
      params: {
        cityName: keyword
      }
    });

    res.get('http://localhost/WeatherReact/api/current-weather.php')
      .then(function (response) {
          self.setState(
          {
            currentWeather: response.data
          }
        );
      })
      .catch(function (error) {
          console.log(error);
      });
  }

  componentDidMount() {
    this.callApiGetCurrentWeather('');
  }
  render() {
    return (
      <div className="col-md-4 col-md-offset-4">
        <h1>Where are you?</h1>

         {/* Include Form */}

        <Form parentApp = {this} />

        {/*Show data in table*/}

        <h3>
          {this.state.currentWeather.location}
        </h3>
        <h4>
          {this.state.currentWeather.bringWhat}
        </h4>
        <h4>
         {this.state.currentWeather.howAllDay}
        </h4>
        <table className="table table-bordered">
          <thead>
            <tr className="table-info">
              <th>
                <img src={this.state.currentWeather.weatherIcon} alt = "weatherIcon" />
              </th>
              <th>{this.state.currentWeather.weatherDescription}</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Fahrenheits</th>
              <td>{this.state.currentWeather.fDegree}&#x212A;</td>
            </tr>
            <tr className="table-primary">
              <th>Celsius</th>
              <td>{this.state.currentWeather.cDegree}&#8451;</td>
            </tr>
            <tr>
              <th>Humidity</th>
              <td>{this.state.currentWeather.humidity}%</td>
            </tr>
            <tr className="table-info">
              <th>Wind Direction</th>
              <td>
               {this.state.currentWeather.windDirection}
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default App;
