<?php
include ('City.php');
include ('HowToday.php');
include ('Display.php');

class Weather
{
    public $weatherResponse = 'Today has ';
    public $count = 0;

    protected $city;
    protected $howToDay;
    protected $display;
    protected $nowtime;
    function __construct()
    {
        $this->city = new City();
        $this->howToDay = new HowToday();
        $this->display = new Display();
        $this->nowtime = date("Y-m-d H:i:s");
    }

    public function getWeatherByCityName($cityName)
    {
        if (empty($cityName)) {
            $userCityName = $this->city->getCurrentCityName();
        } else {
            $userCityName = $cityName;
        }

        $weatherDataArr = $this->getWeatherDataArr($userCityName, $cityName);

        return $this->getResponseJson($weatherDataArr, $userCityName);
    }

    private function getResponseJson($weatherDataArr, $userCityName)
    {
        $weatherArray = $this->getCurrentFutureArr($weatherDataArr);
        $futureInfo = $weatherArray['future'];
        $currentWeatherInfo = $weatherArray['current'];

        $mainFutureWeather = $futureInfo['weather'][0]['main'];

        $kindDayArr = $this->getAllMainWeather($weatherDataArr);
        
        return $this->display->returnJson($userCityName, $futureInfo, $mainFutureWeather, $kindDayArr, $currentWeatherInfo);
    }

    private function getAllMainWeather($weatherDataArr)
    {
        $kindDayArr = [];
        foreach ($weatherDataArr['list'] as $weatherKind) {
            if ($weatherKind['dt_txt'] > $this->nowtime) {
                $celsiusTemp = round($weatherKind['main']['temp'] - 273.15);
                $allMainWeather = $weatherKind['weather'][0]['main'];

                if (empty(in_array($allMainWeather, $kindDayArr))) {
                    $kindDayArr[$celsiusTemp] = $allMainWeather;
                }
            }
        }

        return $kindDayArr;
    }

    private function getCurrentFutureArr($weatherDataArr)
    {
        $weatherArray = [];

        foreach ($weatherDataArr['list'] as $weather) {
         if ($weather['dt_txt'] > $this->nowtime) {
            $weatherArray['future'] = $weather;
            break;
         }
         $weatherArray['current'] = $weather;
        }

        return $weatherArray;
    }

    private function getWeatherDataArr($userCityName, $cityName)
    {
        $weatherDataJson = $this->getWeatherDataJson($userCityName, $cityName);
        $weatherDataArr = json_decode($weatherDataJson, true);
        
        if ($this->hasDataResponse($weatherDataArr)) {
            die ('No data responsed from API');
        }
        return $weatherDataArr;
    }

    private function getWeatherDataJson($userCityName, $cityName)
    {
        if (empty($cityName)) {
            $weatherDataJson = file_get_contents(
            'http://api.openweathermap.org/data/2.5/forecast?q='. $userCityName .'&appid=01852e7b42b60cbe2295f09e16d3d0b3'
            );
        } else {
            $weatherDataJson = $this->getDataJsonById($cityName);
        }
        return $weatherDataJson;
    }

    private function getDataJsonById($cityName)
    {
        $idListDataArr = $this->city->getCityList();
        $userCityName = $cityName;

        foreach ($idListDataArr as $key => $city) {
            if ($city['name'] == $userCityName) {
                $userCityId = $city['id'];
                $weatherDataJson = file_get_contents(
                    'http://api.openweathermap.org/data/2.5/forecast?id='. $userCityId .'&appid=01852e7b42b60cbe2295f09e16d3d0b3'
                );
            }
        }

        return $weatherDataJson;
    }

    private function hasDataResponse($weatherDataArr)
    {
        return empty($weatherDataArr) || empty($weatherDataArr['list'][0]);
    }
}
