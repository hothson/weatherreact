<?php
class HowToday
{
	public $weatherResponse = 'Today';
	public $count = 0;

	private function isToday(){
		if (isset($sky['21']) && $sky['21'] == "clear" && $len == 1) {
			return true;
		}
	}

	public function Clear($sky)
	{
		$weatherResponse = ', sometime sky is '.$sky;

		return $weatherResponse;
	}

	private function checkRainSnowClouds($sky)
	{
		return (stripos($sky, "rain") !==false || stripos($sky, "snow") !== false || stripos($sky, "cloud") !== false);
	}

	// Replace last "," to "and"
	function str_lreplace($search, $replace, $subject)
	{
	    $pos = strrpos($subject, $search);

	    if ($pos !== false) {
	        $subject = substr_replace($subject, $replace, $pos, strlen($search));
	    }

	    return $subject;
	}

	public function howAllDay($kindDayArr)
	{
		$weatherResponse = $this->weatherResponse;
		$snowRainClouds = [];
		$html = '';
		foreach ($kindDayArr as $sky) {
			if ($this->isToday() == true) {
				$html .= "It is a nice day out";
			} else {
				if (stripos($sky, "clear") !== false) {
					$html .= $this->Clear($sky);
				}
				// Put clouds, rain, snow into array
				if ($this->checkRainSnowClouds($sky)) {
					$snowRainClouds[] = $sky;
				}
			}
		}
		$final = $this->str_lreplace(",", " and", implode(', ', $snowRainClouds));
		$html = 'Today has '. $final . $html;

		return $html;
	}
}
