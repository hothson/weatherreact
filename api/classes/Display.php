<?php
class Display
{
    protected $howToDay;
    public $arr = ["NNE","NE","ENE","E","ESE", "SE", "SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"];
    
    function __construct()
    {
        $this->howToDay = new HowToday();
    }
    public function returnJson($userCityName, $futureInfo, $mainFutureWeather, $kindDayArr, $currentWeatherInfo)
    {
        return json_encode([
            'location' => $userCityName,
            'bringWhat' => $this->bringWhat($futureInfo, $mainFutureWeather),
            'howAllDay' => $this->howToDay->howAllDay($kindDayArr),
            'weatherIcon' => $this->getIconUrl($currentWeatherInfo),
            'weatherDescription' => $this->upperCaseDiscription($currentWeatherInfo),
            'fDegree' => $this->getFDegree($currentWeatherInfo),
            'cDegree' => $this->getCelsiusDegree($currentWeatherInfo),
            'humidity' => $currentWeatherInfo['main']['humidity'],
            'windDirection' => $this->degToDirection($currentWeatherInfo['wind']['deg']),
            ]);
    }
    
    private function upperCaseDiscription($currentWeatherInfo)
    {
        return ucwords($currentWeatherInfo['weather'][0]['description']);
    }

    private function getCelsiusDegree($currentWeatherInfo)
    {
        return round($currentWeatherInfo['main']['temp'] - 273.15, 1);
    }

    private function getFDegree($currentWeatherInfo)
    {
        return round(1.8*($currentWeatherInfo['main']['temp'] - 273) + 32);
    }

    private function getIconUrl($currentWeatherInfo)
    {
        return 'http://openweathermap.org/img/w/'. $currentWeatherInfo['weather'][0]['icon'] . '.png';
    }

    private function degToDirection($num)
    {
        $arr = $this->arr;
        if ($num > 348.75 || $num <= 11.25) {
            return "N";
        } elseif (is_int($num/22.5) == true) {
            $val= floor(($num/22.5)-.5);
            return $arr[$val - 1];
        } else {
            $val= floor(($num/22.5)-.5);
            return $arr[($val)];
        }
    }
    
    private function bringWhat($futureInfo, $mainFutureWeather)
    {
        if (!empty($futureInfo) && !empty($mainFutureWeather)) {
            if (stripos($mainFutureWeather, "rain") !== false) {
                return "You may need to bring an Umbrella in the next 3 hours";
            } elseif (stripos($mainFutureWeather, "snow") !== false) {
                return "You may need to bring a Coat in the next 3 hours";
            }
        }
    }
}