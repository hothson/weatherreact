<?php
class City {
	protected $jsonFile;

	function __construct() {
		$this->jsonFile = dirname(dirname(dirname(__FILE__))).'/data/city.list.json';
	}

	public function getCityList()
	{
        $idListData = file_get_contents($this->jsonFile);

        return json_decode($idListData, true);
	}
	
	public function getCurrentCityName()
    {
        $userLocation = file_get_contents('http://ip-api.com/json');
        $userLocationArr = json_decode($userLocation, true);

        return $userLocationArr['city'];
    }

	public function getSuggesionCity($keyword)
	{
		if (empty($keyword)) {
			return [];
		}

		$idListDataArr = $this->getCityList();
		$responseArr = [];
		$numOfResult = 0;

		if(!empty($keyword)) {
			foreach($idListDataArr as $city) {
				if (stripos($city['name'], $keyword) !== false) {
					$responseArr[] = [
						'name'=> $city["name"],
						'id'=> $city["id"],
					];

					if ($numOfResult >= 5) {
						break;
					}

					++$numOfResult;
				}
			}
		}

		return $responseArr;
	}
}