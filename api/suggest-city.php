<?php 
include (dirname(__FILE__).'/classes/configs.php');
include (dirname(__FILE__).'/classes/City.php');

class SuggestCity {
	function getResponse()
	{

		$city = new City();
		$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';

		$responseArr = $city->getSuggesionCity($keyword);

		die (json_encode($responseArr));
	}
}

$a = new SuggestCity();
$a->getResponse();