<?php
include (dirname(__FILE__).'/classes/configs.php');
include (dirname(__FILE__).'/classes/Weather.php');

$weather = new Weather();
$cityName = isset($_GET['cityName']) ? $_GET['cityName'] : '';

$response = $weather->getWeatherByCityName($cityName);

// Die json
die($response);
